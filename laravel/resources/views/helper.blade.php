@if(Session::get('success'))
<div class="alert alert-success" role="alert" id="success-alert">
    <strong>{{Session::get('success')}} </strong>
</div>
@endif
@if(Session::get('danger'))
<div class="alert alert-danger" role="alert">
    <strong>{{Session::get('danger')}} </strong>
</div>
@endif

@if(session('msg'))
<div class="alert alert-success">
    <strong>
        {{session('msg')}}
    </strong>
</div>
@endif

@if(session('logout'))
<div class="alert alert-danger" role="alert">
    <strong>
        {{session('logout')}}
    </strong>
</div>
@endif



<script src="{{asset('asset/js/jquery.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $(".alert").fadeTo(2000, 2000).slideUp(2000, function() {
            $(".alert").slideUp(5000);
        });
    });
</script>