@extends('dashboard')
@section('content')
<div class="form-row">
    <div class="form-group col-md-2">
        <a type="button" class="btn btn-primary " href="{{route('addNewCategory')}}">Add New Category</a>
    </div>
    <div class="form-group col-md-7">
        <input type="text" class="form-control"  placeholder="Search Category" name="cat_search" id="cat_search">
    </div>
    <div class="form-group col-md-3">
        <a  class="form-control col-md-3 btn btn-warning" href="{{route('categoryListing')}}" >Reset All</a>
        
    </div>
</div>
<table id="example" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Id</th>
            <th>category Name</th>
            <th>No of products</th>
            <th>image</th>
            <th>order</th>
            <th>status</th>
            <th>Create Date</th>
            <th>Update Date</th>
            <th colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($category as $key=>$value)
        <tr>
            <td>{{$value->id}}</td>
            <td>{{$value->c_name}}</td>
            <td>{{$value->noOfProduct}}</td>
            <td><img src="{{adminAssets('img/category/'.$value->c_file)}}" width="70px" height="70px"></td>
            <td>{{$value->order}}</td>
            <td>{{$value->status}}</td>
            <td>{{date('d-m-Y', strtotime($value->created_at))}}</td>
            <td>{{date('d-m-Y', strtotime($value->updated_at))}}</td>
            <td><a href="catEdit/{{$value->id}}" class="btn btn-success">Edit</a></td>
            <td><a onclick="return confirm('Are You Sure Delete?')" href="{{route('catDelete', $value->id)}}" class="btn btn-danger">Delete</a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<script src="{{adminAssets('js/jquery.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        //when press enter then run delete code
        document.addEventListener("keydown", function(e) {
            if (e.keyCode === 13) {
                var catname = $("#cat_search").val()
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('catname')}}',
                    data: {
                        catname: catname
                    },
                    success: function(response) {
                        console.log(response);
                        $('tbody').html(response);
                    },
                });
            }
        });

    });

    function catDelete(id) {
        let a = confirm("Are You Sure wan't to delete This Record");
        if (a == true) {
            $.ajax({
                type: "get",
                url: "catDelete/" + id,
                success: function(response) {
                    location.reload();
                }
            });
        } else {
            console.log("record is safe");
        }
    }

    function reset()
    {
         document.getElementById("cat_search").value = "";
    }
</script>
@endsection