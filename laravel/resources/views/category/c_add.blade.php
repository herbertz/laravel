@extends('dashboard')
@section('content')
<form action="{{isset(request()->id)?route('catEdit',$category->id):route('addNewCategory')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="form-group">
        <label for="c_name">Category Name</label>
        <input type="text" class="form-control" id="c_name" name="c_name" value="{{isset(request()->id)?$category->c_name:''}}" placeholder="Enter Category Name">
        @error('c_name')
        <span class="text text-danger">category name must be unique</span>
        @enderror
    </div>
    <div class="form-group">

        <label for="c_file">Category file</label>
        <input type="file" class="form-control" value="{{isset(request()->id)?($category->c_file):''}}" id="c_file" name="c_file">
        @if(request()->id)
        <img src="{{adminAssets('img/category/'.$category->c_file)}}" height="70px" width="70px" alt="img not">
        @endif
    </div>
    <div class="form-group">
        <label for="order">Category Order</label>
        <input type="text" value="{{isset(request()->id)?$category->order:''}}" class="form-control" id="order" name="order" placeholder="Category order">
    </div>

    <div class="form-group">
        <label for="inputState">category Status </label>
        <select id="inputState" name="status" class="js-example-basic-single form-control">
            <option value="Active" {{isset(request()->id)?($category->status=='Active'?'selected':''):''}}>Active</option>
            <option value="Inactive" {{isset(request()->id)?($category->status=='Inactive'? 'selected':''):''}}>Inactive</option>
        </select>
    </div>
    <input type="submit"  class="btn btn-primary" value="{{isset(request()->id)?'Update Category':'Submit'}}">
</form>
<script src="{{adminAssets('js/jquery.min.js')}}"></script>
<link href="{{adminAssets('css/select2.min.css')}}" rel="stylesheet" />
<script src="{{adminAssets('js/select2.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $(".alert").fadeTo(2000, 2000).slideUp(2000, function() {
            $(".alert").slideUp(5000);
        });

        $('.js-example-basic-single').select2();
    });
</script>
@endsection