<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Server-side processing</title>
    
</head>

<body>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    <div class="container">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>c_name</th>
                    <th>order</th>
                    <th>status</th>
                    <th>created_at </th>
                    <th>updated_at</th>
                </tr>
            </thead>
        </table>
    </div>
    
</body>

<script src="{{asset('asset/js/jquery.min.js')}}"></script>
<script src="{{asset('asset/js/jquery.dataTables.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('catSererSide')}}',
                dataType: 'json',
                type: 'GET'
            },
            
            columns: [
                {data: 'id'},
                {data: 'c_name'},
                {data: 'order'},
                {data: 'status'},
                {data: 'created_at'},
                {data: 'updated_at'}
            ]
            
        });
    });
</script>

</html>