<!DOCTYPE html>
<html lang="en">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Server-side processing</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
</head>

<body>
<div class="container">
    <table id="example" class="table table-bordered table-striped" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Salary </th>
                <th>Age</th>
                
            </tr>
        </thead>
        <tfoot>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Salary</th>
                    <th>Age</th>
                </tr>
        </tfoot>
    </table>
</div>
</body>
<script src="{{asset('asset/js/jquery.min.js')}}"></script>
    <script src="{{asset('asset/js/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing : true,
                serverSide : true,
                ajax : {
                    url : '{{ route('person.fetch')}}',
                    dataType : 'json',
                    type : 'GET'
                },
                columns : [
                    { data : 'id' },
                    { data : 'name' },
                    { data : 'salary' },
                    { data : 'age' },
                ]
            });
        } );
    </script>
</html>