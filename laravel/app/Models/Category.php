<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $table = 'categorys';    

    public function prodCount()
    {
        return $this->belongsTo(Product::class);
    }
}