<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticable;

class User extends Authenticable
{
    protected $table = 'users';

    public $timestamps = false;

    protected $filldata = [
        'name',
        'email',
        'password',
        
    ];
}
