<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function index()
    {
        $category = DB::table('categorys')
        -> leftJoin('products','categorys.id', '=', 'products.cid')
        ->selectRaw('categorys.*, count(products.cid) as noOfProduct')
        ->groupBy('categorys.c_name','categorys.id','categorys.c_file','categorys.order','categorys.status','categorys.created_at','categorys.updated_at')
        ->get();

        
         return view('category.c_listing', ['category' => $category]);
    }

    public function create()
    {
        return view('category.c_add');
    }

    public function store(Request $request)
    {
        $catValidate = $request->validate([
            'c_name' => 'required|unique:categorys',
            'c_file' => 'image|mimes:jpeg,png,jpg|max:2048',
            'order' => 'required',
            'status' => 'required',
        ]);

        $data = $request->all();
        $c_insert = new Category();
        $c_insert->c_name = $data['c_name'];
        $fileName = time() . '.' . $request->c_file->extension();
        $request->c_file->move(public_path('/asset/img/category'), $fileName);
        $c_insert->order = $data['order'];
        $c_insert->c_file = $fileName;
        $c_insert->status = $data['status'];
        $c_insert->save();
        return redirect('categoryListing')->with('success', 'Category Added Sucessfully');
    }

    public function edit($id)
    {
        $c_edit = Category::find($id);
        return view('category.c_add', ['category' => $c_edit]);
    }
    public function update(Request $request, $id)
    {
        $image = $request->c_file;
        $cat_validate = $request->validate([
            'c_name' => 'required',
            'order' => 'required',
            'status' => 'required',
        ]);

        $status = $request->input('status');
        $order = $request->input('order');
        $c_name = $request->input('c_name');

        if ($image != null) {
            $fileName = time() . '.' . $request->c_file->extension();
            $request->c_file->move(public_path('/asset/img/category'), $fileName);
            Category::where('id', $id)->update([
                'c_name' => $c_name,
                'order' => $order,
                'c_file' => $fileName,
                'status' => $status,
            ]);
        } else {
            Category::where('id', $id)->update([
                'c_name' => $c_name,
                'order' => $order,
                'status' => $status,
            ]);
        }
        return redirect('categoryListing')->with('success', 'Category Update Successfylly');
    }
    public function destroy($id)
    {
        $cat_delete = Category::find($id);
        unlink(public_path('asset/img/category/' . $cat_delete->c_file));
        $cat_delete->delete();
        return redirect('categoryListing')->with('danger', 'Category Deleted Successfully');
    }

    public function catShow(Request $request)
    {
        if ($request->ajax()) {
            $cat_record = Category::where('c_name', 'LIKE', '%' . $request->catname . "%")->get();
            if ($cat_record) {
                $output = "";
                $img = url('asset/img/category/');
                foreach ($cat_record as $key => $product) {
                    $output .= '<tr>' .
                        '<td>' . $product->id . '</td>' .
                        '<td>' . $product->c_name . '</td>' .
                        '<td> <img src=' . $img . "/" . $product->c_file . ' width=70px height=70px></td>' .
                        '<td>' . $product->order . '</td>' .
                        '<td>' . $product->status . '</td>' .
                        '<td>' . $product->created_at->format('d/m/Y') . '</td>' .
                        '<td>' . $product->updated_at->format('d/m/Y') . '</td>' .
                        '<td> <a class ="btn btn-success" href=catEdit/' . $product->id . '> Edit</a></td>' .
                        '<td> <a class ="btn btn-danger" href=catDelete/' . $product->id . '>Delete</a></td>' .

                        '</tr>';
                }
                return Response($output);
            }
        }
    }

    //this functino is to show the category at server side 
    public function fetchAllCat()
    {
        return view('serverside.category');
    }
    public function fetch(Request $req)
    {
        $col_order = ['id', 'c_name', 'order', 'status','created_at','updated_at'];
        $total_data  = Category::count();
        // dd($total_data);

        $limit = $req->input('length');
        $start = $req->input('start');
        $order = $col_order[$req->input('order.0.column')];
        $dir = $req->input('order.0.dir');

        if (empty($req->input('search.value'))) {
            $post = Category::offset($start)->limit($limit)
                ->orderBy($order, $dir)
                ->get();
            $total_filtered = Category::count();
        } else {
            $search = $req->input('search.value');
            $post = Category::where('id', 'like', "%{$search}%")
            ->orWhere('c_name', 'like', "%{$search}%")
            ->orWhere('order', 'like', "%{$search}%")
            ->orWhere('status', 'like', "%{$search}%")
            ->orWhere('created_at', 'like', "%{$search}%")
            ->orWhere('updated_at', 'like', "%{$search}%")
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();
                
            $total_filtered = Category::where('id', 'like', "%{$search}%")
                ->orWhere('c_name', 'like', "%{$search}%")
                ->orWhere('order', 'like', "%{$search}%")
                ->orWhere('status', 'like', "%{$search}%")
                ->orWhere('created_at', 'like', "%{$search}%")
                ->orWhere('updated_at', 'like', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->count();
        }

        $data = array();
        if ($post) {
            foreach ($post as $row) {
                $nest['id']     = $row->id;
                $nest['c_name']   = $row->c_name;
                $nest['order'] = $row->order;
                $nest['status'] = $row->status;
                $nest['created_at'] = $row->created_at->format('d/m/Y');
                $nest['updated_at']    = $row->updated_at->format('d/m/Y');
                $data[] = $nest;
            }
        }

        $json = array(
            'draw'              =>  intval($req->input('draw')),
            'recordsTotal'      =>  intval($total_data),
            'recordsFiltered'   =>  intval($total_filtered),
            'data'              =>  $data
        );

        echo json_encode($json);
    }
}
